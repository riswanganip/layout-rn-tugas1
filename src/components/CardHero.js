import React from 'react';

import {View, Text, Image, StyleSheet, Dimensions} from 'react-native';

const CardHero = () => {
  return (
    <View style={styles.cardContainer}>
      <Image
        style={styles.imageStyle}
        source={require('../assest/binar.png')}
      />
      <Text style={styles.titleStyle}>Styling di React Native</Text>
      <Text style={styles.subTitle}>Binar Academy - React Native</Text>
      <Text style={styles.contentText}>
        As a components grows, in complexity it is much cleaner and efficient to
        use StyleSheet.create son as to define several styles in one place
      </Text>
      <View style={styles.flexContent}>
        <Text style={{color: '#357C3C'}}>Understood!</Text>
        <Text style={{color: '#357C3C'}}>What?!!</Text>
      </View>
    </View>
  );
};

const deviceWidth = Math.round(Dimensions.get('window').width);
const radius = 20;
const styles = StyleSheet.create({
  cardContainer: {
    marginTop: 10,
    width: deviceWidth - 25,
    marginBottom: 10,
    height: 600,
    borderRadius: radius,
    backgroundColor: '#fff',

    shadowColor: '#000',
    shadowOffset: {
      width: 5,
      height: 5,
    },
    shadowOpacity: 0.75,
    shadowRadius: 5,
    elevation: 9,
  },
  imageStyle: {
    height: 400,
    width: deviceWidth - 25,
    borderTopLeftRadius: radius,
    borderTopRightRadius: radius,
  },
  titleStyle: {
    fontWeight: '500',
    fontSize: 16,
    textAlign: 'center',
    color: '#000',
  },
  subTitle: {
    textAlign: 'center',
    fontSize: 14,
  },
  contentText: {
    textAlign: 'justify',
    fontSize: 16,
    color: '#000',
    paddingLeft: 8,
    paddingRight: 8,
  },
  flexContent: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 30,
    justifyContent: 'space-around',
  },
});

export default CardHero;
