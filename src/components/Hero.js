import React from 'react';
// import {Node} from 'react';
import {StyleSheet, View} from 'react-native';

import CardHero from './CardHero';

const Hero = () => {
  return (
    <View style={styles.body}>
      <CardHero />
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
  },
});

export default Hero;
