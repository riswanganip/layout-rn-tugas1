/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Hero from './src/components/Hero';

const App = () => {
  return <Hero />;
};

export default App;
